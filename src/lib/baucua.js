import { writable } from "svelte/store";

export const DefaultState = {
    /* 
        stages
            roundstart
            betting
            rewarding
    */
    stage: "roundstart",
    player_list: [
        initPlayer("Harold", "🗿", 1000),
        initPlayer("Anna", "😈", 1300),
        initPlayer("Brian", "🍙", 1900),
    ],
    tiles: {
        fish:0,
        prawn:0,
        crab:0,
        rooster:0,
        gourd:0,
        stag:0
    },
    better: initPlayer(),
    tile_rolls: {
        fish:0,
        prawn:0,
        crab:0,
        rooster:0,
        gourd:0,
        stag:0
    }
}

export const state = writable(DefaultState);

export function initPlayer(name, emoji, bank) {
    return {
        name,
        emoji,
        bank,
        balance: bank,
        bets: {
            fish:0,
            prawn:0,
            crab:0,
            rooster:0,
            gourd:0,
            stag:0
        }
    }
}

state.subscribe(value => {
    // console.log(value);
})

